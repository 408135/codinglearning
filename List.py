# List is a data type more info on what data types are: https://www.w3schools.com/python/python_datatypes.asp
# List is given a variable with data in it

# example
List1=["This is awesome"]

# As you can see the List is in the variable

# You can also add on to Lists

# example

List2=[]
List2+="Hello"

# You need += to add on to it with the variable in front of it

# More Information on lists: https://www.w3schools.com/python/python_lists.asp 
