# Variable is a word that you can set up and you can assign it
# You can store Letters/Words

# Heres An Example

MyName="My Name Is Mohammad"

# You can name a Variable anything you want

# You can also add numbers in variables

# Another Example

MyAge=13

# More Information on variables: https://www.w3schools.com/python/python_variables.asp